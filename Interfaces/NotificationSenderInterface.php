<?php


namespace Interfaces;


use Models\Notifications\Notification;
use Models\User;

interface NotificationSenderInterface
{
    public function send(Notification $notification, User $user): bool;
}