<?php
use Actions\Senders\EmailSender;
use Actions\Senders\SmsSender;
use Actions\Senders\PushSender;
use Actions\NotificationService;
use Models\Notifications\Notification;

$emailSender = new EmailSender();
$smsSender = new SmsSender();
$pushSender = new PushSender();

$notificationService = new NotificationService([$emailSender, $smsSender, $pushSender]);

$users = []; //Массив пользователей
$notification = new Notification("Купи!!!");

$notificationService->sendNotification($notification, $users);