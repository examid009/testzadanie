<?php

namespace Models;

class User
{
    protected string $name;
    protected string $email;
    protected string $phone;

    protected function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    protected function setEmail(string $email): self
    {
        $this->email = $email;
        return $this;
    }

    protected function setPhone(string $phone): self
    {
        $this->phone = $phone;
        return $this;
    }
}