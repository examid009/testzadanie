<?php

namespace Models\Notifications;

/**
 * Class Notification
 * @package Models\Notifications
 */
class Notification
{
    protected string $title;
    protected string $message;
    protected ?array $meta = null; //Доп метаданные мб

    public function __construct($message)
    {
        $this->message = $message;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;
        return $this;
    }

    public function setMeta(array $meta): self
    {
        $this->meta = $meta;
        return $this;
    }
}