<?php

namespace Actions\Senders;

use Interfaces\NotificationSenderInterface;
use Models\Notifications\Notification;
use Models\User;

class EmailSender implements NotificationSenderInterface
{
    public function send(Notification $notification, User $user): bool
    {
        // Реализация отправки e-mail
        return true;
    }
}