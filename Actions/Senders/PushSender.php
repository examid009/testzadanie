<?php

namespace Actions\Senders;

use Interfaces\NotificationSenderInterface;
use Models\Notifications\Notification;
use Models\User;

class PushSender implements NotificationSenderInterface
{
    const ANDROID = 1;
    const IOS = 2;

    protected ?int $platform = null;

    public function setPlatform(int $platform): self
    {
        $this->platform = $platform;
        return $this;
    }

    public function send(Notification $notification, User $user): bool
    {
        if (!empty($this->platform)) {
            if($this->platform == self::ANDROID) {
                //Настроить ПУШ под андройд
            } else if ($this->platform == self::IOS) {
                //Настроить ПУШ под IOS
            } else {
                throw new \Exception("Неизвестная платформа");
            }
        }
        // Реализация отправки push
        return true;
    }
}