<?php

namespace Actions\Senders;

use Interfaces\NotificationSenderInterface;
use Models\Notifications\Notification;
use Models\User;

class SmsSender implements NotificationSenderInterface
{
    public function send(Notification $notification, User $user): bool
    {
        // Реализация отправки sms
        return true;
    }
}