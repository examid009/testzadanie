<?php

namespace Actions;

use \Models\Notifications\Notification;

class NotificationService
{
    /**
     * @var array - Массив интерфейсов Interfaces/NotificationSenderInterface.php
     */
    private array $senders;

    public function __construct(array $senders)
    {
        $this->senders = $senders;
    }

    /**
     * @param Notification $notification
     * @param array $users - массив класса Models/User.php
     */
    public function sendNotification(Notification $notification, array $users): void
    {
        foreach ($users as $user) {
            foreach ($this->senders as $sender) {
                if ($sender->send($notification, $user)) {
                    /**
                     * Одно уведомление должно быть доставлено только один раз, даже если оно поддерживается несколькими способами доставки.
                     * Если я правильно понял задачу. А так мне кажется если уж отправлять, то на все
                     */
                    break;
                }
            }
        }
    }
}